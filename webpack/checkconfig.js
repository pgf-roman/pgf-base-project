const inq = require('inquirer');
const fs = require('fs-extra');
const _ = require('lodash');
const validPath = require('is-valid-path');
const validUrl = require('valid-url').isUri;
const config_path = './config.json';

const promise = function(){
	let resolve, reject;
	let q = new Promise((res, rej) => {
		resolve = res;
		reject = rej;
	});
	return {q: q, resolve: resolve, reject: reject};
};

function prompt() {
    return new Promise((resolve, reject) => {
        const questions = [
            {
                name: 'devUrl',
                type: 'input',
                message: 'BrowserSync proxy url e.g. localhost/project-name, http://dev-project-a.dmz.pgf.com.pl',
                validate: answer => {
                    return new Promise((res, rej) => {
                        if (answer.indexOf('http://') < 0 && answer.length > 0) {
                            answer = 'http://' + answer;
                        }
                        if (!validUrl(answer) || _.isEmpty(answer)) {
                            return rej('Invalid url');
                        }
                        res(true);
                    });
                }
            }, 
           {
               name: 'context',
               type: 'input',
               message: 'Project root e.g. H:project_name_a',
           }
		];

        inq.prompt(questions).then((answers) => {
            answers.entry = [
                "./assets/scripts/main.js"
                // "./assets/styles/main.scss"
              ];
            fs.outputJson(config_path, answers, err => {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        });
    });
};


function checkConfig() {
    const q = promise();
    function checkFile(path) {
		const q = promise();
		fs.stat(path, (err, stat) => {
			if (err) {
				return q.resolve(err);
			}
			return q.resolve();
		});
		return q.q;
	}
    
    checkFile(config_path).then(err => {
        if (err) {
            if (err.code === 'ENOENT') {
                console.error('No config file!');
                return prompt().then(q.resolve);
            }
            return q.reject(err);
        }
        return q.resolve();
    });
    return q.q;
       
}

module.exports = checkConfig();
