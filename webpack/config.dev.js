const baseWebpackConfig = require('./config.base');
const config = require('../config');
const merge = require('webpack-merge');
const path = require('path');
const portscanner = require('portscanner');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

let port = 3000;
portscanner.findAPortNotInUse(3000, 3100, config.devUrl, (err, found_port) => {
    port = found_port;
});

module.exports = (env) => merge(baseWebpackConfig, {
    watch: env.dev,
    devtool: 'cheap-module-source-map',
    plugins: [
      new BrowserSyncPlugin({
            files: [path.resolve(config.context, './views/**/*.twig'), path.resolve(config.context, './public_html/dist/*.*'), ],
            injectChanges: true,
            port: port,
            proxy: {
                target: config.devUrl
            }
        }, {
            reload: false
        }),
    ],
});
