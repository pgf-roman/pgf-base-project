const webpack = require('webpack');
const path = require('path');
const postcssReporter = require('postcss-reporter');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const autoprefixer = require('autoprefixer');
const notifier = require('node-notifier');
const globImporter = require('node-sass-glob-importer');
const config = require('../config');
const {sync} = require('glob');
const _ = require('lodash');
const glob = sync;
const CopyWebpackPlugin = require('copy-webpack-plugin');

config.entry = config.entry.map(entry => path.resolve(config.context, entry))
const postcssConfig = require('./postcss.config.js');

const extractStyles = new ExtractTextPlugin({
    filename: '[name].css'
});

const supportedBrowsers = [
    '> 0.5%',
    'last 2 versions',
    'not ie <= 10',
];


const scssProcessors = [
    autoprefixer({
        browsers: supportedBrowsers,
        cascade: false,
    })
];

module.exports = {
    stats: {
        errorDetails: false,
        colors: true
    },
    // context: path.resolve(config.context),
    entry: config.entry,
    output: {
        path: path.resolve(config.context, 'public_html/dist'),
        filename: '[name].js',
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            babelrc: false,
                            cacheDirectory: true,
                            presets: [require('babel-preset-es2015')], 
                            plugins: ['transform-runtime', 'babel-polyfill']
                        },
                    },
                ],
            },
            {
                test: /\.(css|scss)$/,
                use: extractStyles.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true,
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: postcssConfig,
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                                plugins: scssProcessors,
                                importer: globImporter()
                            },
                        },
                    ],
                }),
                // options: {publicPath: 'styles'}
            },
            {
                test: /\.(woff2?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]',
                        // publicPath: '',
                        // useRelativePath: true,
                    },
                }, ],
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'images/[name].[ext]',
                            // useRelativePath: true,
                        },
                    },
//                    {
//                        loader: 'image-webpack-loader',
//                        options: {
//                            mozjpeg: {
//                                progressive: true,
//                                quality: 65
//                            },
//                            // optipng.enabled: false will disable optipng
//                            optipng: {
//                                enabled: false,
//                            },
//                            pngquant: {
//                                quality: '65-90',
//                                speed: 4
//                            },
//                            gifsicle: {
//                                interlaced: false,
//                            },
//                            // the webp option will enable WEBP
//                            webp: {
//                                quality: 75
//                            },                            
//                        },
//                    },
                ],
            }
        ],
    },
    plugins: [
//        new webpack.optimize.CommonsChunkPlugin({
//            name: "common",
//        }),
    new CopyWebpackPlugin([
        {
            from: path.resolve(config.context, 'assets/images'),
            to: path.resolve(config.context, 'public_html/dist/images')
        }
    ]),
        extractStyles,
        new webpack.NoEmitOnErrorsPlugin(),
        new FriendlyErrorsPlugin({
            clearConsole: 0,
            onErrors: (severity, errors) => {
                if (severity === 'error') {
                    const error = errors[0];
                    return notifier.notify({
                        //                        title: context.pkg.name,
                        message: severity + ': ' + error.name,
                        subtitle: error.file || '',
                        //                        icon: ICON
                    });
                }
            }
        })
    ],
};
