const baseWebpackConfig = require('./config.base');
const merge = require('webpack-merge');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractStyles = new ExtractTextPlugin({
    filename: '[name].css'
});

module.exports = (env) => merge(baseWebpackConfig, {
    watch: false,
    devtool: false,
    module: {
        rules: [
//             {
//                test: /\.scss$/,
//                use: extractStyles.extract({
//                    use: [{
//                            loader: 'clean-css-loader',
//                            options: {
//                                sourceMap: true,
//                                compatibility: 'ie10',
//                                format: 'beautify',
//                                level: {
//                                    2: {
//                                        mergeMedia: true,
//                                    }
//                                }
//                            }
//                        }],
//            publicPath: path.resolve(config.context, '../'),
//                }),
//            }
        ]
    },
    plugins: [
        new UglifyJsPlugin(),
        extractStyles,
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: { removeAll: true } },
            canPrint: true
        })
    ],
});