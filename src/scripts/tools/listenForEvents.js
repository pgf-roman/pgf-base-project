const $ = require('jquery');
const _ = require('lodash');

module.exports = function (events) {
	let evs = [];
	for (let ev in events) {
		evs.push(ev);
		$(document).on(ev, function(e) {
			const args = _.drop(arguments);
			if (_.isFunction(events[ev])) {
				events[ev].apply(PGFApp, _.concat(e, args));
			}
		});
	}
	return ['listening for', evs];
};
