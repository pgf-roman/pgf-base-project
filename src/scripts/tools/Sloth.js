const _ = require('lodash');
const $ = require('jquery');

module.exports = class {
	constructor($elems) {
		this.$elems = $elems;
	}

	init() {
		this.$elems.each((i, el) => this.cacheSrcSet($(el)));
		$(document).on('TD.resize', e => this.resize(e));
	}

	cacheSrcSet($$) {
		const srcset_data = [];
		const srcset = $$.attr('data-srcset').split(',');
		_.forEach(srcset, size => {
			size = size.split(' ');
			const [src, width] = [size[0], size[1].replace('w', '')];
			srcset_data.push({
				src: src,
				width: width
			});
		});
		$$.data('srcset', srcset_data);
	}

	resize(e) {
		this.$elems.each((i, el) => this.setSrc($(el)));
	}

	setSrc($$) {
		const srcset = $$.data('srcset');
		const target = _.minBy(srcset, set => {
			return set.width < TD.window_w;
		});

		if (!$$.is("img")) {

			$$.css("background-image", `url(${target.src})`);

		} else {

			$$.attr("src", target.src);
			//			$$.on('load', e => all_loaded, promise?);

		}
	}
}
