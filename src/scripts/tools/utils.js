class Utils {
	get platform() {
		return {
			Android: function () {
				return !!navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function () {
				return !!navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function () {
				return !!navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function () {
				return !!navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function () {
				return !!navigator.userAgent.match(/IEMobile/i);
			}
		}
	}
	get isMobile() {
		return this.platform.iOS() || this.platform.Android() || this.platform.BlackBerry() || this.platform.Opera() || this.platform.Windows();
	}
}

module.exports = Utils;