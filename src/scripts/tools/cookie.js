class Cookie {
    static set(key, value, hours) {
        try {
            let expires = "";
            if (hours) {
                let date = new Date();
                date.setTime(+date + (hours * 60 * 60 * 1000));
                expires = date.toGMTString();
            }
            window.document.cookie = key + "=" + value + "; expires=" + expires + "; path=/";
        } catch (e) {
            console.warn(e);
        }
        return value;
    }
    
    static get(key) {
        try {
            let name_eq = key + "=";
            let ca = document.cookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(name_eq) === 0) {
                    return c.substring(name_eq.length, c.length);
                }
            }
        } catch (e) {
            console.warn(e);
        }
        return null;
    }
    
    static remove(key, val) {
        Cookie.set(key, val, 'Fri, 01 Jan 2010 12:00:00 UTC');
    }
    
    static storageClear() {
        localStorage.clear();
    }
    
    static storageClean() {
        this.storageClear();
    }
    
    static storageAdd(item, val) {
        if (_.isEmpty(item) || _.isEmpty(val)) {
            console.warn('Item: ' + item + ' and value: ' + val + ' are empty');
        } else {
            return localStorage.setItem(item, val);
        }
    }
    
    static storageRemove(item) {
        if (_.isEmpty(item)) {
            console.warn('No parameters passed to remove, use storageClean to clean');
        } else {
            localStorage.removeItem(item);
        }
    }
    
    static storageGet(key) {
        if (_.isEmpty(key)) {
            console.warn('Item: ' + key + ' is empty');
        } else {
            return localStorage.getItem(key);
        }
    }
}

module.exports = Cookie;