const _ = require('lodash');
const $ = require('jquery');
/*
    @param {jQuery Object} jQuery wrap
    @param {json} json object
    
    use data-bind="param: jsonpath, attr[attribute_name]: jsonpath"
    param can be:
        text - text inside that element
        html - inputs string as a html inside that element
        value - changes inputs value
        attr[attribute name] - changes attribute of that element
    
    <div data-bindit>
        <div data-bind="text: test, class: class"></div>
        <input data-bind="value: nested.val">
        <a data-bind="attr[href]: nested.array.[0].link"></a>
    </div>

*/
module.exports = class {
    constructor($wrap, data) {
        this.$wrap = $wrap;
        this.data = _.cloneDeep(data); 
        this._hook()._refresh();
    }

    set(data) {
        this.prev_data = _.cloneDeep(this.data);
        this.data = _.cloneDeep(data);
        this._refresh();
        return this;
    }

    _refresh() {
        _.each(this.bindings, (obj) => {
            const prev_val = _.get(this.prev_data, obj.path);
            let val = this._getVal(obj.path);
            if (_.isEmpty(`${val}`) || _.isUndefined(val)) {
                val = '';
            }
            if (prev_val === val) {
                return 0;
            }
            switch (obj.type) {
                case 'value': 
                    obj.element.val(val);
                    break;
                case 'text':
                    obj.element.text(val);
                    break;
                case 'html':
                    obj.element.html(val);
                    break;
                case 'class':
                    obj.element.removeClass(prev_val);
                    obj.element.addClass(val);
                    break;
                default:
                    if (obj.type.indexOf('attr') >= 0) {
                        const attr = obj.type.substring(obj.type.lastIndexOf("[") + 1, obj.type.lastIndexOf("]"));
                        obj.element.attr(attr, val);
                    }
           }
        });
        return this;
    }

    _hook() {
        this.bindings = [];
        const $elems = this.$wrap.find('[data-bind]');
        $elems.each((i, el) => {
            const $$ = $(el);
            const data_A = $$.data().bind.split(',');
            _.forEach(data_A, data => {
                data = data.split(':');
                const obj = {
                    element: $$,
                    type: data[0].trim(),
                    path: data[1].trim(),
                };
                this.bindings.push(obj);
            });
        });
        return this;
    }

    _getVal(path) {
        if (!!path) {
            return _.get(this.data, path);
        } else {
            return null;
        }
    }
};