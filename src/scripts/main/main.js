import _Utils from '../tools/Utils';
import _PGFApp from './PGFApp';
require.context("../../svg/", true, /^\.\/.*\.svg/); // load svg

(function (window) {
	window.Utils = new _Utils();
	window.deb = function (s) {
		if (window.location.hostname == "localhost" ||
			window.location.href.indexOf("dev") > 0 ||
			window.location.href.indexOf(":3000") > 0) {
			console.log.apply(console, arguments);
		}
	}

	window.PGFApp = new _PGFApp();

	document.addEventListener('DOMContentLoaded', e => window.PGFApp.state.ready(e));
	window.addEventListener('load', e => window.PGFApp.state.load(e)); 
})(window, document);