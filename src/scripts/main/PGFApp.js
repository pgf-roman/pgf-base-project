import $ from 'jquery';
import _ from 'lodash';

class PGFApp {
	constructor() {
		this.is_local = window.location.hostname == "localhost" ? true : false;
		this.breakpoints = require('../../breakpoints');
		this.regx = {
			email: new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")
		};
		this.anim_t = 250;
	}

	get deps() {
		return {
			Sample : require('../partials/Sample')
		};
	}

	loadPartial(name, $parent = $('body')) { 
		if (!_.hasIn(this, `${name}_A`)) {
			this[`${name}_A`] = []; 
		}
		$parent.find(`[data-${name.toLowerCase()}]`)
			.add($parent.filter(`[data-${name.toLowerCase()}]`))
			.each((i, el) => {
				const partial = new this.deps[name]($(el));
				this[`${name}_A`].push(partial);
				partial.init();
			});
	};

	loadPartials($parent) {
		_.forEach(this.deps, (name, key) => {
			this.loadPartial(key, $parent);
		});
	}

	get state() {
		return {
			ready: e => {
				this.events();
				$(window).on("resize", _.debounce(e => $(document).trigger('PGFApp.resize', e), 100));
				$(window).on("scroll", _.throttle(e => $(document).trigger('PGFApp.scroll'), 50));
				$(document).on("mousemove", _.throttle(e => $(document).trigger('PGFApp.mousemove'), 100));
				$('[data-to-top]').on('click', e => $('html, body').animate({
					scrollTop: '0px'
				}, 500)); 

				this.loadPartials(); //load all partials
				this.require();
				$(document).trigger('PGFApp.resize'); //always trigger this last
			},
			load: e => {
				//				deb(e);
			}
		}
	}

	require() {
		const Sloth = require('../tools/Sloth');
		this.responsiveImgs = new Sloth($('[data-srcset]'));
		this.responsiveImgs.init();
	}

	events() {
		const listenForEvents = require('../tools/listenForEvents');
		const events = {
			"PGFApp.resize": e => {
				this.window_w = Utils.isMobile ? window.screen.width : window.innerWidth;
				this.window_h = window.innerHeight;
				this.callDeps('resize', e); //call all partials with method resize
//				deb(e);
			}, 
			"PGFApp.scroll": e => {
//				deb(e);
			},
			"PGFApp.mousemove": e => {
//				deb(e);
			},
			"PGFApp.imagesLoaded": e => {

			}
		};
		const listener = listenForEvents(events);
		deb(listener);
	}
	
	callDeps(func_name) {
		// deb(arguments, func_name);
		let args = _.drop(arguments);
		_.forEach(this.deps, (name, key) => {
			_.forEach(this[`${key}_A`], obj => {
				if (_.hasIn(obj, func_name)) {
					obj[func_name].apply(obj, arguments);
				}
			});
		});
	}
}

module.exports = PGFApp;
