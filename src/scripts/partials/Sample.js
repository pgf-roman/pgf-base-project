const $ = require('jquery');
const _ = require('lodash');
const dom = require('../main/cacheDOM');
const Bindy = require('../tools/Bindy');

module.exports = class {
    constructor($wrap) {
        this.$wrap = $wrap;
        this.data = $wrap.data('sample');
    }

    init() {
        const elements = {
            something: '[d-something]'
        };
        this.dom = new dom(this.$wrap);
        this.dom.cache(elements);

    }

    getData() {
        return JSON.stringify(this.data); 
    }

    resize(e) {
        //this method will fire on end of resize event   
        this.dom.$something.width($('[data-some-elem]').width())
    }
}
