class Menu {
    constructor() {
        let elements = {
            toggle: '[d-toggle]',
            occluder: '[d-occluder]'
        };
        this.dom = new dom($('[data-header]'));
        this.dom.cache(elements);
        this.$toggle = $toggle;
        this.$occluder = $occluder;
        this.$toggle.on("click", this.toggleMobileMenu.bind(this));
    }
    toggleMobileMenu(e) {
        this.dom.$root.toggleClass("active");
        if (this.dom.$root.hasClass("active")) {
            this.dom.$root.slideDown();
            this.dom.$occluder.fadeIn();
        } else {
            this.dom.$root.slideUp();
            this.dom.$occluder.fadeOut();
        }
    }
    menuHandler(e) {
        let scrolltop = $("body").scrollTop();
        let proximity = false,
            needed = false;
        try {
            proximity = e.clientY < 160;
        } catch (e) {}
        if (scrolltop < 140 || proximity) {
            needed = true;
        }
        if (!needed) {
            PGFApp.dom.$header.addClass("folded");
        } else {
            PGFApp.dom.$header.removeClass("folded");
        }
    }
}
