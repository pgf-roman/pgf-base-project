# PGF front-end webpack based build tool

## What purpose does it serve?

This is task runner/ build tool that has wide range of application, JS es6 babel trabspilation, scss with globs and postcss plugins, images/sprites minifcation, svg compilation, browsersync
## How do I set up?

 - open console inside projects folder and input  `npm i` and wait for installation to end
 - input `npm start`
 - prompt will ask for dev server url
 - optional - `project.config.json` file will be created in the projects root, you can edit it manually, for example change entry files paths, default is `src/styles/main.scss` and `src/scripts/main/main.js`
 Then the builder will watch for changes inside `src` and compile files to `dist` folder.

## Usage

### Using 3rd-party libraries
To add a vendor library, use `npm i -S bootstrap` command inside projects root.
### Styles
To import from **node_modules**, use `~`sing, e.g. ` @import "~bootstrap/scss/bootstrap.scss"`
To import a whole directory, you can use a glob pattern `@import: 'settings/**/*.scss';`
Images are added to a sprite. Sprites file is generated inside `sprites/main.scss`
### Scripts
Babel transpiler is used. You can use **ES6** features like **import/export** statements, destructive assignment, map, etc.
### ~~SVG/IMG~~ 
doesn't work on PGF webdev server
SVGO is installed, all files inside `src/svg` will get optimised. What files get optimised is declared inside main app js entry file. `require.context("../../svg/", true, /^\.\/.*\.svg/);`
Files like jpg, png etc. can be loaded analogicaly. Otherwise, paths will be extracted from scss/js.

## Scripts
To run dev env use `npm start` command. To build project, use `npm build` command. After **merge to master** use `npm  run build:prod`. This will minify code and remove sourcemaps.